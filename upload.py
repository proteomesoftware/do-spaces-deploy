import os
import boto3


def main():

    session = boto3.session.Session()
    client = session.client('s3',
                            endpoint_url='https://sfo2.digitaloceanspaces.com',
                            region_name='sfo2',
                            aws_access_key_id=os.getenv('SPACES_ID'),
                            aws_secret_access_key=os.getenv('SPACES_SECRET'))

    client.put_object(Bucket='proteomesoftware-test',
                      Key='hello-world.txt',
                      Body=b'Hello, World!',
                      ACL='public')


if __name__ == '__main__':
    main()
