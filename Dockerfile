FROM python:alpine3.14

RUN apk update && apk add ca-certificates bash && rm -rf /var/cache/apk/*

COPY upload.py /
COPY pipe.sh /

RUN /bin/bash -c 'chmod +x /upload.py'
RUN /bin/bash -c 'chmod +x /pipe.sh'

ENTRYPOINT ["/pipe.sh", "/upload.py"]
